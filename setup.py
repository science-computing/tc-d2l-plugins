try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
setup(name='testcreator-d2l-plugins',
    version='0.1',
    description='Importer / Exporter for Testcreator / D2L Interop',
    #author='Ryan Goggin',
    #author_email='ryan.goggin@uwaterloo.ca',
    #url='https://ryangoggin.net',
    packages=find_packages(),
    include_package_data=True,
)

