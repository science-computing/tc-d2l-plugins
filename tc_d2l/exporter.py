import io
import json
import re
import zipfile
from uuid import uuid4
from django.template.loader import render_to_string
from testcreator.exporters.base import BaseExporter
from testcreator.exporters.utilities import (
    HtmlProcessor,
    standard_html_styles,
    convert_equations_to_mathml,
)


class Processor(HtmlProcessor):
    pre_process = [
        lambda x: re.sub(
            r"src=\"\/image\/view\/(.*?)\"",
            r'src="\1.png" style="max-width: 100%"',
            x
        ),
    ]
    bs4_process = [
        convert_equations_to_mathml,
        standard_html_styles
    ]


def cleaner(htmlstr):
    return Processor(htmlstr).process()


class D2LExporter(BaseExporter):
    slug = "d2l"
    name = "D2L Quiz"
    content_type = "application/zip"
    extension = "zip"
    detail = """Exports to a quiz which can be imported by D2L
    (LEARN). To import the quiz...."""
    notices = ["Does not currently support exporting SOLUTIONS or FEEDBACK"]
    supported_question_types = {
        "native_mc": {
            'notices': ["Doesn't support pinning order of questions / answers"]
        }
    }

    def export_test(self):
        test = self.test
        self.uuid = str(uuid4())
        self.test_data = {
            "uid": self.uuid,
            "cover_sheet": {
                "content": cleaner(test.coversheet),
                "title": test.title if test.title != '' else 'Untitled',
            },
            "feedback": {},
            "question_groups": [],
        }

    def export_group(self, group, gi):
        g = {
            "ident": f"SECT_{self.uuid}_{gi}",
            "title": "",
            "content": cleaner(group.content),
            "scramble_questions": group.shuffle,
            "feedback": {},
            "questions": [],
        }
        self.current_group = g
        self.test_data["question_groups"].append(g)

    def export_question_instance(self, instance, iindex):
        guid = self.current_group["ident"]
        q = {
            "ident": f"QUES_{guid}_{iindex}",
            "global_id": str(uuid4()),
            "title": "",
        }
        self.current_group['questions'].append(q)
        self.current_q = q

    def get_file(self):
        output = render_to_string(
            'tc_d2l/test_template.xml',
            self.test_data
        )
        manifest = render_to_string(
            'tc_d2l/imsmanifest.xml',
            self.test_data
        )

        outfile = io.BytesIO()
        with zipfile.ZipFile(outfile, "w") as z:
            z.writestr("quiz_data.xml", output)
            z.writestr("imsmanifest.xml", manifest)

            for image in self.test.image_set.all():
                z.write(image.file.path, "{}.png".format(image.id))
        outfile.seek(0)
        return outfile

    def export_native_mc(self, question, instance, *args, **kwargs):
        body = question.body
        config = instance.config
        quid = self.current_q["ident"]
        self.current_q.update(
            {
                "content": cleaner(body["content"]),
                "scramble_answers": config["shuffle"],
                "fix_order": False,
                "answers": [
                    {
                        "ident": f"{quid}_{ai}",
                        "correct": a["correct"],
                        "content": cleaner(a["content"]),
                        "fix_order": False,
                    }
                    for ai, a in enumerate(body["answers"])
                ],
                "scoring": {
                    "correct": float(instance.max_value),
                    "incorrect": 0,
                    "partial": False,
                    "num_correct": 1,
                },
            }
        )
